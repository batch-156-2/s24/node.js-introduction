// MongoDB Logo: natural and simple
console.log('Hello Batch 156!');

/*
	http (hypertext transfer protocol): is a built in module of NodeJS that will allow us to establish a connection and allow us to transfer data over the HTTP.

	Use a function called require() in order to acquire the desired module. Repackage the module and install it inside a new variable.
*/

let http = require('http');

	// Http will provide us with all the components needed to establish a server.
	// createServer(): will allow us to create a server that follows the HTTP server protocol.
	// Provide/Instruct the server what to do.
		//Try printing a message.
	// Identify and describe a location where the connection will happen, in order to provide a proper medium for both parties.

let port = 3000; 

	// Listen(): bind and listen to a specific port whenever its being accessed by your computer.

	// Identifying a point where we want to terminate the transmission using the end() function.

http.createServer( function(request , response) {
	response.write('I like you alot.');
	response.end()
}).listen(port);


	// Create a response in the terminal to confirm if a connection has been successfully established.
		//1. We can give a confirmation to the client that a connection is working.
		//2. We can specify what address/location the connection was established.
	// We will use template literals

console.log(`I like you too, please dont have anyone else: ${port}`);

	// 1. Initialize an npm into your local project
		// Syntax: npm init
		// shortcut Syntax: npm init -y

	// package.json: "the Heart of any Node projects", it records all the important metadata about the project, (libraries, packages, functions) that makes up the system.

	// 2. Install 'nodemon' into our project
		// Skills:
			// 1. Install: npm install <dependency>
			// 1. Uninstall: npm uninstall <dependency>
			//				 npm un <dependencies>
		// Installing multiple:
			// Dependencies:
				// npm i <list of dependencies>
					// express

	// 3. Run your app using the nodemon utility engine.
		// npm install -g(global): the library is being installed globally into your machine's file system so that the program will be recognizable accross	your file system.
		// (YOU ONLY HAVE TO EXECUTE -G COMMAND ON NODEMON ONCE)
		// nodemon: is a CLI utility tool; its task is to wrap your node.js app and watch for any changes in the file system and automatically restarts or hotfixes the process.
		
// .gitignore: you will be able to successfully push your node_modules in your remote repo.

// Repercussions
	// It will take a longer time for you to stage and push your projects online.
	// It will cause an error during deployment because most hosting platform rejects node_modules during the building stage.

// How to fix if accidentally pushing node_modules?
	// git rm(remove) -r(recursive) --cached node_modules
	// Recursive: involving a program or routine that describes explicit interpretations of a component/module/file.



